import React from 'react';
import { NavLink } from 'react-router-dom';

const Navigation = () => {
    return (
        <div className='navigation'>
            <NavLink exact to="/" activeClassName="nav-active">Accueil</NavLink>
            <NavLink exact to="/a-propos" activeClassName="nav-active">A propos</NavLink>
            <NavLink exact to="/donnees" activeClassName="nav-active">Données</NavLink>
            <NavLink exact to="/personel" activeClassName="nav-active">Personel</NavLink>
            <NavLink exact to="/statistique" activeClassName="nav-active">Statistique</NavLink>
        </div>
    );
};

export default Navigation;