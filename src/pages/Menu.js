import React from 'react';

const Menu = () => {
    return (
      <div>
        <nav id="tmSidebar" className="tm-bg-black-transparent tm-sidebar">
          <button
            className="navbar-toggler"
            type="button"
            aria-label="Toggle navigation"
          >
            <i className="fas fa-bars"></i>
          </button>
          <div className="tm-sidebar-sticky">
            <div className="tm-brand-box">
              <div className="tm-double-border-1">
                <div className="tm-double-border-2">
                  <h3 className="tm-brand text-uppercase">
                    service des examens et de la certification
                  </h3>
                </div>
              </div>
            </div>
            <ul
              id="tmMainNav"
              className="nav flex-column text-uppercase text-right tm-main-nav"
            >
              <li className="nav-item">
                <a href="#intro" className="nav-link active">
                  <span className="d-inline-block mr-3">Intro</span>
                  <span className="d-inline-block tm-white-rect"></span>
                </a>
              </li>
              <li className="nav-item">
                <a href="#propos" className="nav-link">
                  <span className="d-inline-block mr-3">A propos</span>
                  <span className="d-inline-block tm-white-rect"></span>
                </a>
              </li>
              <li className="nav-item">
                <a href="#donnees" className="nav-link">
                  <span className="d-inline-block mr-3">Données</span>
                  <span className="d-inline-block tm-white-rect"></span>
                </a>
              </li>
              <li className="nav-item">
                <a href="#liste" className="nav-link">
                  <span className="d-inline-block mr-3">Listes</span>
                  <span className="d-inline-block tm-white-rect"></span>
                </a>
              </li>
              <li className="nav-item">
                <a href="#statistique" className="nav-link">
                  <span className="d-inline-block mr-3">Statistique</span>
                  <span className="d-inline-block tm-white-rect"></span>
                </a>
              </li>
            </ul>
            <footer className="text-center text-white small">
              <p className="mb--0 mb-2">Copyright 2022</p>
              <p className="mb-0">
                Design:
                <a
                  rel="nofollow"
                  href="https://templatemo.com"
                  className="tm-footer-link"
                >
                  Template Mo
                </a>
              </p>
            </footer>
          </div>
        </nav>
      </div>
    );
};

export default Menu;