import React from 'react';

const Introduction = () => {
    return (
        <div
              className="parallax-window"
              data-parallax="scroll"
              data-image-src="img/fianarantsoa1.jpg">
              <div className="tm-section-wrap">
                <section id="intro" className="tm-section">
                  <div className="col-xl-6">
                    <div className="tm-section-half" style={{padding: "0px"}}>
                      <div className="tm-bg-white-transparent tm-intro">
                        <h2 className="tm-section-title mb-5 text-uppercase tm-color-primary">Dirigeants DREN Haute Matsiatra depuis 2009</h2>
                          <p className="tm-color-gray" style={{fontSize: "14px"}}>
                            <b>Les différentes dirigeants du DREN Haute Matsiatra :</b> <br/>
                            <ul style={{listStyleType:"square", fontSize: "14px"}}>
                              <li className="tm-color-gray">RAKOTONANDRASANA Marcel</li>
                              <li className="tm-color-gray">RAZAFINDRAKOTO Victor </li>
                              <li className="tm-color-gray">RANDRIAMPARALAIMAHARAVO Aimé Martin </li>
                            </ul>
                          </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-6">
                    <div className="tm-section-half" style={{padding: "0px"}}>
                      <div className="tm-bg-white-transparent tm-intro">
                        <h2 className="tm-section-title mb-5 text-uppercase tm-color-primary">Introduction Service des examens et de la certification</h2>
                        <p className="tm-color-gray" style={{fontSize: "14px"}}>
                          Sur l'examen, cette service 
                          fait l'organistion matériels et logiciels pour les examens CEPE/6ème et
                          supervise pendant les examens 
                          ainsi les examens BEPC/2nde. <br/>
                        </p>
                        <p className="tm-color-gray" style={{fontSize: "14px"}}>
                          Ainsi, il reçoit les demandes de diplômes CAP/EP et CAE/EP. <br/>
                        </p>
                        <p className="mb-0 tm-color-gray" style={{fontSize: "14px"}}>
                          Sur la certification, cette service certifie les diplômes suivants: <br/>
                          <ul style={{listStyleType:"circle", fontSize: "14px"}}>
                            <li className="tm-color-gray">diplôme CEPE</li>
                            <li className="tm-color-gray">diplôme CAP/EP</li>
                            <li className="tm-color-gray">diplôme CAE/EP</li>
                          </ul>
                        </p>
                      </div> 
                    </div>
                  </div>      
                </section>
            </div>            
          </div>
    );
};

export default Introduction;