import React from 'react';
import About from './About';
import Data from './Data';
import Introduction from './Introduction';
import Liste from './Liste';
import Menu from './Menu';
import Statistique from './Statistique';

const Home = () => {
    return (
      <main class="container-fluid">
        <div class="row">
          <Menu />
          <main role="main" class="ml-sm-auto col-12">
            <Introduction />
            <About />
            <Data />
            <Liste />
            <Statistique />
          </main>
        </div>
      </main>
    );
};

export default Home;