const Donnees = () =>{
    return (
      <div className="tm-section-wrap bg-white">
        <section id="donnees" className="row tm-section">
          <div className="col-12">
            <div className="w-100 tm-double-border-1 tm-border-gray">
              <div className="tm-double-border-2 tm-border-gray tm-box-pad">
                <div className="tm-gallery-wrap">
                  <h2 className="tm-color-primary tm-section-title mb-4 ml-2">
                    Ajout données
                  </h2>
                  <iframe
                    src="/cepe/service/ajoutDonnees.php"
                    style="height: 500px;"
                    frameborder="0"
                    className="embed-responsive"
                    scrolling="no"
                  ></iframe>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
};

export default Donnees;
