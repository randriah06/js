import React from 'react';

const Statistique = () => {
    return (
      <div className="tm-section-wrap bg-white stat">
        <section id="statistique" className="row tm-section">
          <div className="col-12 tm-section-pad">
            <div className="tm-flex-item-left">
              <div className="tm-w-100">
                <h2 className="tm-color-primary tm-section-title mb-4">
                  Statistique
                </h2>
                <iframe
                  title="myframeStat"
                  className="embed-responsive"
                  style={{ height: "800px" }}
                  src="/cepe/statistique/cisco/cisco.php"
                  frameborder="0"
                  scrolling="no"
                ></iframe>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
};

export default Statistique;