import React from 'react';

const About = () => {
    return (
            <div className="tm-section-wrap bg-white">
            <section id="propos" className="row tm-section">
              <div className="col-xl-6">
                <div className="tm-section-half">    
                    <div><i className="fas fa-6x fa-home mb-5 tm-section-icon"></i></div>                        
                    <h2 className="tm-section-title tm-color-primary mb-5">A propos</h2>
                    <p className="mb-5">
                      La Direction Régionale de l’Education Nationale 
                      a été créée durant la période de la Première République. 
                      Mais ce nom du direction n'est apparu qu'en 2004.  
                      Son siège se trouve maintenant à Mahamanina.
                    </p>
                </div>
              </div>
              <div className="col-xl-6">
                <div className="tm-section-half">
                    <div><i className="far fa-6x fa-building mb-5 tm-section-icon"></i></div>
                    <h2 className="tm-section-title tm-color-primary mb-5">Contact</h2>
                    <p className="mb-5">
                      <i className="fa fa-phone"></i> téléphone : <br/>
                      <i className="fab fa-facebook"></i> facebook : <br/>
                      <i className="fab fa-twitter"></i> twitter : <br/>
                      <i className="fa fa-envelope"></i> e-mail : 
                    </p>
                    <p>
                      <i className="fa fa-location-dot"></i> siège : <i>Mahamanina(capre)</i>
                    </p>
                </div>
              </div>
            </section>
          </div>
    );
};

export default About;