import React from 'react';

const Data = () => {
    return (
      <div className="tm-section-wrap bg-white">
        <section id="donnees" className="row tm-section">
          <div className="col-12">
            <div className="w-100 tm-double-border-1 tm-border-gray">
              <div className="tm-double-border-2 tm-border-gray tm-box-pad">
                <div className="tm-gallery-wrap">
                  <h2 className="tm-color-primary tm-section-title mb-4 ml-2">
                    Ajout données
                  </h2>
                  {/* <iframe
                    title='myframeData'
                    src="/cepe/service/ajoutDonnees.php"
                    style={{height: "500px"}}
                    frameborder="0"
                    className="embed-responsive"
                    scrolling="no"
                  ></iframe> */}
                  <div className="bg-white">
                    <h3>Personnel</h3>
                    <form>
                      <div className="row">
                        
                        <div className="col-lg-6">
                          <div className="row">
                            <div className="col-lg-12">
                              <div className="row">
                                <div className="col-lg-6 col-md-6 col-sm-12">
                                  <label htmlFor="name">Nom:</label>
                                  <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    placeholder="Newman"
                                  ></input>
                                </div>

                                <div className="col-lg-6 col-md-6 col-sm-12">
                                  <label htmlFor="prenom">Prénom:</label>
                                  <input
                                    type="text"
                                    className="form-control"
                                    id="prenom"
                                    placeholder="Marcel"
                                  ></input>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-12">
                              <label htmlFor="im">IM:</label>
                              <input
                                type="email"
                                className="form-control"
                                id="im"
                                placeholder="0000"
                              ></input>
                            </div>
                            <div className="col-lg-12">
                              <label htmlFor="phone">Téléphone: </label>
                              <input
                                type="text"
                                className="form-control"
                                id="phone"
                                placeholder="+261348675352"
                              ></input>
                            </div>
                            <div className="col-lg-12">
                              <label htmlFor="poste">
                                Nom du poste:
                              </label>
                              <input
                                type="text"
                                className="form-control"
                                id="poste"
                                placeholder="Chef"
                              ></input>
                            </div>
                            <div className="col-lg-12">
                              <label htmlFor="porte">
                                Numéro du porte:
                              </label>
                              <input
                                type="text"
                                className="form-control"
                                id="porte"
                                placeholder="12"
                              ></input>
                            </div>

                            
                              <button
                                type="submit"
                              >
                                Enregistrer
                              </button>
                            </div>
                          </div>
                        </div>
                      
                    </form>
                    <h3>Statistique</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
};

export default Data;