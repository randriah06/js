import React from 'react';

const Liste = () => {
    return (
      <div className="tm-section-wrap bg-white">
        <section id="liste" className="row tm-section">
          <div className="col-12 tm-section-pad" style={{ height: "auto" }}>
            <div className="tm-flex-item-left">
              <div className="tm-w-100">
                <h2 className="tm-color-primary tm-section-title mb-4">
                  Listes
                </h2>
                <iframe
                  title="myframeListe"
                  src="../service/listeDonnees.php"
                  style={{ height: "640px" }}
                  frameborder="0"
                  className="embed-responsive"
                  scrolling="no"
                ></iframe>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
};

export default Liste;