# image de base
FROM node:latest 

# repertoire de travail
WORKDIR /app

# copier fichier necessaire
COPY package*.json ./
COPY . .

# installer les dependances
RUN npm install

# commande pour lancer
RUN npm start

# port application
EXPOSE 3000

# commande par defaut
CMD ["node", "index.js"] 